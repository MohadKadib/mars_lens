//
//  MarsService.swift
//  MarsLens
//
//  Created by mohammed abdulla kadib on 3/10/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import Foundation
import RxSwift


protocol MarsServiceProtocol {
    func fetchMarsPhotos() -> Observable<[MarsPhotoCollection]>
}

class MarsService: MarsServiceProtocol {
    
    
    //Function to fetch the photos from the API and return them as an observableObject
    func fetchMarsPhotos() -> Observable<[MarsPhotoCollection]> {
        
        return Observable.create { (observer) -> Disposable in
            
            let task = URLSession.shared.dataTask(with: URL(string: "https://api.nasa.gov/mars-photos/api/v1/rovers/curiosity/photos?sol=1000&camera=fhaz&api_key=DEMO_KEY")!) { (data, _, _) in
                
                guard let Mydata = data else {
                    observer.onError(NSError(domain: "", code: -1, userInfo: nil))
                    return
                }
                
                do {
                    let marsPhotos = try JSONDecoder().decode(MarsPhotoCollection.self, from: Mydata)
                    observer.onNext([marsPhotos])
                    
                } catch {
                    observer.onError(error)
                }
                
            }
            
            task.resume()
            
            return Disposables.create{
                task.cancel()
            }
            
        }
        
    }
    
}
