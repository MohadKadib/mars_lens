//
//  MarsPhotoViewModel.swift
//  MarsLens
//
//  Created by mohammed abdulla kadib on 3/10/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit

struct MarsPhotoViewModel {
    
    
    private let marsPhoto: MarsPhotoCollection
    
    var displayText: String {
        return marsPhoto.photos[0].earth_date
    }
    
    
    
    init(marsPhoto: MarsPhotoCollection) {
        
        self.marsPhoto = marsPhoto

    }
    
}
