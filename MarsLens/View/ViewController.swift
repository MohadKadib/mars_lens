//
//  ViewController.swift
//  MarsLens
//
//  Created by mohammed abdulla kadib on 3/10/20.
//  Copyright © 2020 mohammed abdulla kadib. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ViewController: UIViewController {
    
    let disposeBag = DisposeBag()
    private var viewModel: MarsListViewModel!
    
    
    @IBOutlet weak var tableView: UITableView!
    
    static func instantiate(viewModel: MarsListViewModel) -> ViewController {
        let storyBoard = UIStoryboard(name: "Main", bundle: .main)
        let viewController = storyBoard.instantiateInitialViewController() as! ViewController
        viewController.viewModel = viewModel
        return viewController
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        
        viewModel.fetchMarsPhotoViewModels().observeOn(MainScheduler.instance).bind(to:
        tableView.rx.items(cellIdentifier: "cell")) { index, viewModel, cell in
            
            cell.textLabel?.text = viewModel.displayText
        }.disposed(by: disposeBag)
        
    }


}

